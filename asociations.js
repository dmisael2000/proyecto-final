const Productos = require("models").productos;
const Precio = require("models").precio;

Productos.hasOne(Precio, { as: "producto", foreignKey: "producto_id" });
//Añade una clave productoId a la tabla precio
Precio.belongsTo(Productos, { as: "producto", foreignKey: "producto_id" });