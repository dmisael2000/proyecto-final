"use strict";

const bcrypt = require("bcrypt");

const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class clientes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  clientes.init(
    {
      nombre: DataTypes.STRING,
      apellido: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      telefono: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "clientes",
    }
  );
  clientes.login = (email, password) => {
    return clientes
      .findOne({ where: { email: email } })
      .then((cliente) => {
        if (!cliente) return null;

        return new Promise((resolve, reject) => {
          //compara 2 contraseñas
          bcrypt.compare(password, cliente.password, (error, valid) => {
            if (error || !valid) reject(error);
            resolve(cliente);
          });
        });
      })
      .catch((error) => console.log(error));
  };
  //Antes de que se cree el registro en la DB
  clientes.beforeCreate((clientes, options) => {
    return new Promise((resolve, reject) => {
      //si no me pasas el password largar error
      if (!clientes.password) {
        reject();
      }
      bcrypt.hash(clientes.password, 10, (error, hash) => {
        clientes.password = hash;
        resolve();
      });
    });
  });
  return clientes;
};
