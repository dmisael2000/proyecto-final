'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class productos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  productos.init({
    name: DataTypes.STRING,
    price_1000: DataTypes.INTEGER,
    price_500: DataTypes.INTEGER,
    price_250: DataTypes.INTEGER,
    category_id: DataTypes.INTEGER,
    img_name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'productos',
  });
  return productos;
};