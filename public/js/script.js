const btnProductos = document.querySelector('.btnProductos')
const btnPedidos = document.querySelector('.btnPedidos')
const btnDestacados = document.querySelector('.btnDestacados')
const btnProductos2 = document.querySelector('.btnProductos2')
const btnPedidos2 = document.querySelector('.btnPedidos2')
const btnDestacados2 = document.querySelector('.btnDestacados2')
const campoCambio1 = document.querySelector('.displayProductos')
const campoCambio2 = document.querySelector('.displayPedidos')
const campoCambio3 = document.querySelector('.displayDestacados')

btnProductos.addEventListener('click', (e)=>{
    campoCambio1.classList.remove("ocultar")
    campoCambio2.classList.add("ocultar")
    campoCambio3.classList.add("ocultar")
})

btnPedidos.addEventListener('click', (e)=>{
    campoCambio1.classList.add("ocultar")
    campoCambio2.classList.remove("ocultar")
    campoCambio3.classList.add("ocultar")
})

btnDestacados.addEventListener('click', (e)=>{
    campoCambio1.classList.add("ocultar")
    campoCambio2.classList.add("ocultar")
    campoCambio3.classList.remove("ocultar")
})

btnProductos2.addEventListener('click', (e)=>{
    campoCambio1.classList.remove("ocultar")
    campoCambio2.classList.add("ocultar")
    campoCambio3.classList.add("ocultar")
})

btnPedidos2.addEventListener('click', (e)=>{
    campoCambio1.classList.add("ocultar")
    campoCambio2.classList.remove("ocultar")
    campoCambio3.classList.add("ocultar")
})

btnDestacados2.addEventListener('click', (e)=>{
    campoCambio1.classList.add("ocultar")
    campoCambio2.classList.add("ocultar")
    campoCambio3.classList.remove("ocultar")
})

