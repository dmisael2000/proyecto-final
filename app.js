const express = require("express");
const app = express();
var methodOverride = require('method-override')
const port = process.env.PORT || 3000;
const session = require("express-session");
const mysqlStore = require("express-mysql-session")(session);

// require("asociations");

// conecto la base de datos a mi proyecto

const options = {
  host: "localhost",
  port: 3306,
  user: "root",
  password: "",
  database: "e-commerce",
};

// vinvulo la base de datos con el objeto options
const sessionStore = new mysqlStore(options);

app.use(
  session({
    secret: "sjnsdjsndsnin",
    saveUninitialized: false,
    resave: false,
    store: sessionStore, // guarda los datos en la sessionStore que esta conectada a mi db
  })
);

//Public
app.use(express.static("public"));

//Ejs
app.set("view engine", "ejs");

//Middleware
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(methodOverride('_method'));
app.use(function (req, res, next) {
  res.locals.session = req.session;
  next();
});

//Ruta

app.get("/", (req, res) => {
  res.redirect('/inicio');
});

//Uso de Rutas requeridas
app.use(require("./Routes/registro"));
app.use(require("./Routes/carro"));
app.use(require("./Routes/admin"));
app.use(require("./Routes/categorias"));
app.use(require("./Routes/productos"));


//Escuchando el puerto
app.listen(port, () => {
  console.log(`localhost: ${port}`);

});
