const productoController = require('../controllers/productos');
const carroController = require('../controllers/carro');
const express = require('express');
const router = express.Router();

router.get('/carro', productoController.show)

//Mostrar categorias en el nav

router.get('/carro', carroController.carro)

//agregar producto
router.post('/carro/agregar', (req, res) => {
    console.log(req.body)
    if(!Array.isArray(req.session.carro)){
        req.session.carro = []
    }
    req.session.carro.push({producto_id: req.body.producto_id, cantidad: 1, peso_producto: req.body.peso_producto})
    console.log(req.session.carro)
    res.redirect('/carro')
})

// sumar

router.get('/carro/sumar/:id', (req, res) => {
    console.log(req.body)
    if(!Array.isArray(req.session.carro)){
        req.session.carro = []
    }
    req.session.carro.forEach((element, index) => {
        if(element.producto_id == req.params.id) {
            element.cantidad++
            req.session.carro[index] = element
        }
    });
    console.log(req.session.carro)
    res.redirect('/carro')
})

//restar

router.get('/carro/restar/:id', (req, res) => {
    console.log(req.body)
    if(!Array.isArray(req.session.carro)){
        req.session.carro = []
    }
    req.session.carro.forEach((element, index) => {
        if(element.producto_id == req.params.id) {
            if(element.cantidad > 1){
                element.cantidad--
                req.session.carro[index] = element
            }
        }
    });
    console.log(req.session.carro)
    res.redirect('/carro')
})


//quitar producto

router.get('/carro/quitar/:id', (req, res) => {
    console.log(req.body)
    if(!Array.isArray(req.session.carro)){
        req.session.carro = []
    }
    req.session.carro = req.session.carro.filter(producto => {
        if(producto.producto_id != req.params.id) {
            return true
        }
    });
    console.log(req.session.carro)
    res.redirect('/carro')
})

module.exports = router;