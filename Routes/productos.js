const express = require('express');
const router = express.Router();
const productosController = require("../controllers/productos");

router.get('/productos', productosController.mostrar)

module.exports = router;