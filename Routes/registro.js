const express = require('express');
const router = express.Router();

//traigo el body desde el módulo express validator
const { body } = require('express-validator');

const regController = require('../controllers/registro');

router.get('/registro', regController.register);
//en la creación paso un array con distintas validaciones posibles
router.post('/registro', [
    //de body leo el email, lo saca de views > auth > registro.ejs
    //chequea si el email es un email y si la password tiene un minimo de 5 caracteres
    body('email').isEmail().withMessage('Correo inválido'),
    body('password').isLength({min:5}).withMessage('Debe tener 5 o más caractéres')
], regController.create);

router.get('/', regController.formLogin);
router.post('/', regController.login);

router.get('/logout', regController.logout);


module.exports = router;