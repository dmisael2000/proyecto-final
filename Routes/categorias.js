const categoriaController = require("../controllers/categorias");
const express = require("express");
const router = express.Router();

router.get("/inicio", categoriaController.index)
router.get("/categoria/:id", categoriaController.show);

module.exports = router;
