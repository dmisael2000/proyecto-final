const express = require('express');
const router = express.Router();

//importamos el controlador

const adminController = require('../controllers/admin')

//Usamos el controlador en vez de usar el req y el res

router.get('/admin', adminController.admIndex);

//create me va a mostrar el formulario y el formulario va a enviar info al store
//el create me va a mandar informacion al store
router.get('/admin/create', adminController.admCreate);
//para crear datos con post, se usa el nombre store, quiero guardar un dato dentro de categorias
//ir a controllers>categorias y agregar store

router.post('/admin', adminController.admStore);

//Modificar un elemento
router.get('/admin/:id', adminController.admShow);

//Modificación
//tiene 2 métodos, get y put
//el get muestra un formulario con información
router.get('/admin/:id/edit', adminController.admEdit);
router.put('/admin/:id', adminController.admUpdate);

//Eliminar

router.delete('/admin/:id', adminController.admDestroy);

module.exports = router;