const { Op } = require("sequelize");
const productoModel = require("../models").productos;
const precioModel = require("../models").precio;

module.exports = {
  show: async (req, res) => {
    if (!Array.isArray(req.session.carro)) {
      req.session.carro = [];
    }
    const productos_ids = req.session.carro.map(
      (producto) => producto.producto_id
    );
    const productos = [];
    productoModel.findAll({
        where: {
          id: {
            [Op.in]: productos_ids,
          },
        },
      })
      .then((rows) => {
        console.log(rows);
        // res.locals.session = req.session
        res.render("carro", { productos: rows });
      })
      .catch((error) => console.log(error));
    // await new Promise ((res, rej) => {
    //     req.session.carro.forEach( async producto => {
    //         const rowProducto = await productoModel.findByPk(producto.producto_id)
    //         const rowPrecio = await precioModel.findOne({where: {producto_id: rowProducto.id, peso: producto.peso_producto}})
    //         productos.push({id: rowProducto.id, nombre: rowProducto.name, precio: rowPrecio.precio, peso: producto.peso_producto })
    //          // .then(row => {
    //          //     productos.push({id: row.id, nombre: row.name, precio: row.price_1, peso: producto.peso_producto })
    //          // })
    //          // .catch(error => console.log(error))
    //      });

    //      res()
    // })
  },
  mostrar: (req, res) => {
    productoModel.findAll()
      .then(prod => {
          res.render('productos/productos', { prod })
      })
      .catch(error => console.log(error))
  }
};
