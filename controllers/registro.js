const clientes = require("../models").clientes;
//me dice como fue la validación
const { validationResult } = require("express-validator");

module.exports = {
  register: (req, res) => {
    res.render("auth/registro");
  },
  create: (req, res) => {
    clientes
      .create({
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        email: req.body.email,
        password: req.body.password,
        telefono: req.body.tel,
      })
      .then((cliente) => {
        req.session.nombreUsuario = req.body.nombre;
        res.redirect("/");
      })
      .catch((error) => console.log(error));
  },
  formLogin: (req, res) => {
    res.render("index");
  },
  login: (req, res) => {
    clientes
      .login(req.body.email, req.body.password)
      .then((cliente) => {
        console.log(cliente);
        if (cliente) {
          req.session.nombreUsuario = cliente.nombre;
          req.session.clientesId = cliente.id;
        }
        res.redirect("/");
      })
      .catch((error) => console.log(error));
  },
  logout: (req, res) => {
    req.session.destroy(() => {
      res.redirect("/");
    });
  },
};
