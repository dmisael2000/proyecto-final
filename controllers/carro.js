const categoriaModel = require("../models").categorias;

module.exports = {
    carro: (req,res) => {
    categoriaModel.findAll()
      .then((rows) => {
        res.render("carro", { rows});
      })
      .catch((error) => console.log(error));
    }
}