const categoriaModel = require("../models").categorias;
const productoModel = require("../models").productos;

module.exports = {
    //Crear categoría
    admIndex: (req, res) => {
        categoriaModel.findAll()
          .then((rows) => {
            res.render("admin/indAdmin", { rows });
          })
          .catch((error) => console.log(error));
      },
      admCreate: (req, res) => {
        res.render('admin/create');
      },
      admStore: (req, res) => {
        categoriaModel.create({ nombre: req.body.nombre})
            .then(result => res.redirect('/admin/'))
            .catch(error => console.log(error))
      },
      //Mostrar
      admShow: (req, res) => {
        categoriaModel.findByPk(req.params.id)
        .then((row) => {
          productoModel.findAll({ where: { category_id: row.id } })
            .then((productos) => { 
              categoriaModel.findAll()
              .then((rows) => {
                res.render("admin/show", { row, productos, rows });
              });
            })
            .catch((error) => console.log(error));
        })
        .catch((error) => console.log(error));
      },
      //Editar
      admEdit: (req, res) => {
        categoriaModel.findByPk(req.params.id)
            .then(row => {
                res.render('admin/edit', { row })
            })
            .catch(error => console.log(error))
      },
      admUpdate: (req, res) => {
        categoriaModel.update({ nombre: req.body.nombre }, { where: { id: req.params.id }})
            .then(() => {
                res.redirect('/admin/' + req.params.id)
            })
            .catch(error => console.log(error))
      },
      //Borrar
      admDestroy: (req, res) => {
        categoriaModel.destroy({ where:{id: req.params.id} })
            .then(() => res.redirect('/admin'))
            .catch(error => console.log(error))
      }
}