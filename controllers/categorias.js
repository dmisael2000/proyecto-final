const categoriaModel = require("../models").categorias;
const productoModel = require("../models").productos;

module.exports = {
  index: (req, res) => {
    categoriaModel.findAll()
      .then((rows) => {
        req.session.texto = "Session iniciada";
        console.log(req.session);
        let errorUser = "";
        let nombre = "login";
        if (!req.session.nombreUsuario) {
          errorUser = "!! Datos ingresados incorrectos";
        } else {
          nombre = req.session.nombreUsuario;
        }
        res.render("index", { rows, nombre, errorUser});
      })
      .catch((error) => console.log(error));
  },
  show: (req, res) => {
    categoriaModel.findByPk(req.params.id)
      .then((row) => {
        console.log(row);
        productoModel.findAll({ where: { category_id: row.id } })
          .then((productos) => { 
            categoriaModel.findAll()
            .then((rows) => {
              console.log(productos);
              res.render("categorias/show", { row, productos, rows });
            });
          })
          .catch((error) => console.log(error));
      })
      .catch((error) => console.log(error));
  }
};
