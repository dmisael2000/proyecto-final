"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert(
      "categorias",
      [
        { id: 1, nombre: "Mix" },
        { id: 2, nombre: "Frutos Secos y Disecados" },
        { id: 3, nombre: "Cereales" },
        { id: 4, nombre: "Harinas" },
        { id: 5, nombre: "Legumbres" },
        { id: 6, nombre: "Semillas" },
        { id: 7, nombre: "Productos Bañados en Chocolate" },
        { id: 8, nombre: "Infusiones" },
        { id: 9, nombre: "Repostería" },
        { id: 10, nombre: "Especias" },
        { id: 11, nombre: "Otros" },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("categorias", null, {});
  },
};
